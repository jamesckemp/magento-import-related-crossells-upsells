<?php
require_once('app/Mage.php');
umask(0);
$app = Mage::app();


// CSV formated like: SKU, Related 1, Related 2, Related 3, Related 4
$row = 1;
if (($handle = fopen("relatedprods.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
    	if($row != 1) {
	        $num = count($data);
	        $skulinks = array();
	        for ($c=0; $c < $num; $c++) {
	        	if($data[$c]) {
		        	// Check if the SKU exists, if it does, add it to our array.
		        	$id = Mage::getModel('catalog/product')->getIdBySku($data[$c]);
					if ($id){
					    $skulinks[] = $data[$c];
					}
	            }
	        }
	        $sku = array_shift($skulinks);
	        if(!empty($skulinks)) {
		        jck_add_product_link($sku, $skulinks);
	        }
        }
        $row++;
    }
    fclose($handle);
}



function jck_add_product_link($sku, $skulinks){
	// $sku is the product to add products links to
	/* $skulinks should be passed in as an array of SKUs. These will be added to the product found with $sku
	$skulinks = array(
		'HG10-001',
		'TP0031',
	    'TG0009'
	);
	*/
	// change LINK_TYPE_RELATED below for other product link types, options are: LINK_TYPE_RELATED/LINK_TYPE_CROSSSELL/LINK_TYPE_UPSELL/LINK_TYPE_GROUPED

	$product = Mage::getModel('catalog/product');
	$productId = $product->getIdBySku($sku);
	$_product = $product->load($productId);
	
	$related = array();
	
	$i = 0; foreach($skulinks as $skulink){
		$productId = $product->getIdBySku($skulink);
		$related[$productId] = array('position' => $i);
	$i++; }
	
	try {
	    $_product->getLinkInstance()->getResource()->saveProductLinks($_product, $related, Mage_Catalog_Model_Product_Link::LINK_TYPE_RELATED);
	    echo "Successfully added links to ".$sku.'<br><br>';
	}
	catch (Exception $ex) {
	    echo $sku.': '.$ex.'<br><br>';
	}
}
?>